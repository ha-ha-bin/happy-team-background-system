from django.contrib import admin
from django.urls import path,include,re_path

# import xadmin
from goods import views
from goods.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('goodslist/', views.index,name='goodsindex'),
    # path('uploadindex/', views.uploadindex,name="uploadindex"),
    path('upload/', views.upload,name="upload"),
    path('typeupload/', views.typeupload,name="typeupload"),
    path('', loginView,name="Login"),#登录
    path('login/', loginView,name="Login"),#登录
    path('reg/', regView,name="Reg"),#注册
    path('logout/', logoutView,name="Longout"),#退出登录
    path('update/', updateView,name="Update"),#修改密码
    path('findpwd/', findpwdView,name="Findpwd"),#找回密码
    # re_path(r'^detail/(?P<goods_id>\d+)/$', views.detailview.as_view(), name="detail"),
    path('api/goodsdata/',goodsdata,name="Goodsdata"),
    path('api/typedata/',typedata,name="typedata"),
]