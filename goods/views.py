import json

from appdirs import unicode
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from goods import models
from goods.models import Goods ,Type
from django.views.generic.base import View
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from random import Random
from django.core.mail import send_mail
from django.http.response import JsonResponse

from django.http import HttpResponse
# Create your views here.


def index(request):
    allgoods = Goods.objects.all() #动态读取

    for i in allgoods:
        print(i)
        print("%d\t%s\t%s\t%s\t%s\t%s\t%s"%(i.id,i.name,i.price,i.goodsdesc,i.weight,i.imgurl,i.classified))
    return render(request, 'index.html',  {
        "goodslist":allgoods,
    })

def goodsdata(request):
    allgoods = Goods.objects.all()
    goods = []
    for i in allgoods:
        goods.append({"name":i.name,"price":i.price,"goodsdesc":i.goodsdesc,"weight":i.weight,"imgurl":"/"+str(i.imgurl),"classified":i.classified})
    return JsonResponse(goods,safe=False)
def typedata(request):
    alltype = Type.objects.all()
    types = []
    for i in alltype:
        types.append({"type":i.type})
    return JsonResponse(types,safe=False)

@csrf_exempt
def upload(request):
    upgoods=[]
    name=request.POST.get('name')
    price=request.POST.get('price')
    goodsdesc=request.POST.get('goodsdesc')
    weight=request.POST.get('weight')
    imgurl=request.FILES.get('imgurl')
    classified=request.POST.get('type')

    upgoods.append({"name":name,"price":price,"goodsdesc":goodsdesc,"weight":weight,"classified":classified})
    print("upgoods:")
    print(upgoods)
    print("imgurl")
    print(imgurl)

    ob=Goods()
    ob.name=name
    ob.price=price
    ob.goodsdesc=goodsdesc
    ob.weight=weight
    ob.imgurl=imgurl
    ob.classified=classified
    ob.save()

    return JsonResponse(upgoods,safe=False)

@csrf_exempt
def typeupload(request):
    upgoods=[]
    name=request.POST.get('name')
    print("name")
    print(name)
    ob=Type()
    ob.type=name
    ob.save()

    return JsonResponse(upgoods,safe=False)
#登录
def loginView(request):
    if request.method=="POST":
        username=request.POST.get("username")
        password=request.POST.get("password")
        if User.objects.filter(username=username):
            user=authenticate(username=username,password=password)
            if user:
                if user.is_active:
                    login(request,user)
                request.session['username']=username
                return redirect("/goodslist/")
                # return render(request,"goods/list.html",{'username':username})
            else:
                msg="用户名密码错误"
        else:
            msg="用户名不存在"
    return render(request, "goods/login.html", locals())
#注册
def regView(request):
    if request.method=="POST":
        username=request.POST.get("username")
        password=request.POST.get("password")
        email=request.POST.get("email")
        if User.objects.filter(username=username):
            msg="用户名已存在"
        else:
            user=User.objects._create_user(username=username,password=password,email=email)
            user.save()
            msg="注册成功"
    return render(request, "goods/register.html", locals())
#退出登录
def logoutView(request):
    logout(request)
    return redirect("/login/")
#修改密码
def updateView(request):
    if request.method=="POST":
        username=request.POST.get("username")
        password=request.POST.get("password")
        newpassword=request.POST.get("newpassword")
        #先验证用户名是否正确
        if User.objects.filter(username=username):
            #验证旧密码是否正确
            user=authenticate(username=username,password=password)
            user.set_password(newpassword)
            user.save()
            msg="密码修改成功"
        else:
            msg="用户不存在"
    return render(request, "user/update.html", locals())
#随机生成验证码
def random_str(randomlength=8):
    str = ''
    chars = 'abcdefghijklmnopqrstuvwsyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    length = len(chars) - 1
    random = Random()
    for i in range(randomlength):
        str += chars[random.randint(0, length)]
    return str

#发送邮件找回密码
def findpwdView(request):
    if request.method=="GET":
        email_title = "找回密码"
        code=random_str()#随机生成的验证码
        request.session["code"]=code #将验证码保存到session
        email_body = "验证码为：{0}".format(code)
        send_status = send_mail(email_title, email_body,"1811189822@qq.com",["1811189822@qq.com",])
        msg="验证码已发送，请查收邮件"
    else:
        username=request.POST.get("username")
        password=request.POST.get("password")
        user=User.objects.get(username=username)
        code=request.POST.get("code") #获取传递过来的验证码
        if code==request.session["code"]:
            user.set_password(password)
            user.save()
            del request.session["code"] #删除session
            msg="密码已重置"
    return render(request, "goods/findpwd.html", locals())