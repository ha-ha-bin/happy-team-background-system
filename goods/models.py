from django.db import models

# Create your models here.
class Goods(models.Model):
    name = models.CharField(max_length=100, verbose_name=u"商品名称")
    price = models.CharField(max_length=100, verbose_name=u"商品价格")
    goodsdesc = models.CharField(max_length=1000, verbose_name=u"商品描述")
    weight = models.CharField(max_length=100, verbose_name=u"单位")
    imgurl=models.FileField(blank=True ,upload_to="static/upload/%Y/%m", verbose_name=u"商品图片", max_length=100)
    classified=models.CharField(max_length=100, verbose_name=u"类型")
class Type(models.Model):
    type=models.CharField(max_length=100, verbose_name=u"商品类型")