# Generated by Django 3.2.5 on 2022-06-02 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Goods',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('goodsname', models.CharField(max_length=100, verbose_name='商品名称')),
                ('goodsprice', models.CharField(max_length=100, verbose_name='商品价格')),
                ('goodsdesc', models.CharField(max_length=1000, verbose_name='商品描述')),
                ('goodsweight', models.CharField(max_length=100, verbose_name='单位')),
                ('goodsimgurl', models.ImageField(upload_to='static/upload/%Y/%m', verbose_name='商品图片')),
            ],
        ),
    ]
