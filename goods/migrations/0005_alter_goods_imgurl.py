# Generated by Django 3.2.5 on 2022-06-09 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0004_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goods',
            name='imgurl',
            field=models.FileField(blank=True, upload_to='static/upload/%Y/%m', verbose_name='商品图片'),
        ),
    ]
