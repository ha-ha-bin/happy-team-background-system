from django.contrib import admin

# Register your models here.
from goods.models import Goods


class goodsadmins(admin.ModelAdmin):
    list_display = ('name', 'price', 'weight')
    list_editable = ('price',)
    search_fields = ('name',)

admin.site.register(Goods,goodsadmins)

